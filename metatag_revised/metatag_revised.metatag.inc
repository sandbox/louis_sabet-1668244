<?php

/**
 * @file
 * Metatag integration for the metatag_revised module.
 */

/**
 * Implements hook_revised_config_default_alter().
 */
function metatag_revised_metatag_config_default_alter(array &$configs) {
  foreach ($configs as &$config) {
    switch ($config->instance) {
      case 'global':
        $config->config += array(
          'revised' => array('value' => '[current-date:custom:Y-m-d\TH:i:s]')
        );
        break;
      case 'global:frontpage':
        $config->config += array(
          'revised' => array('value' => '[current-date:custom:Y-m-d\TH:i:s]')
        );
        break;
      case 'node':
        $config->config += array(
          'revised' => array('value' => '[current-date:custom:Y-m-d\TH:i:s]')
        );
        break;
      case 'taxonomy_term':
        $config->config += array(
          'revised' => array('value' => '[current-date:custom:Y-m-d\TH:i:s]')
        );
        break;
      case 'user':
        $config->config += array(
          'revised' => array('value' => '[current-date:custom:Y-m-d\TH:i:s]')
        );
        if (variable_get('user_pictures')) {
          $config->config += array(
          'revised' => array('value' => '[current-date:custom:Y-m-d\TH:i:s]')
          );
        }

        break;
    }
  }
}

/**
 * Implements hook_metatag_info().
 */
function metatag_revised_metatag_info() {
  $info['groups']['revised'] = array(
    'label' => t('Meta Revised'),
  );

  $info['tags']['revised'] = array(
    'label' => t('Revised Date & Time'),
    'description' => t('The date and time the document was last revised.'),
    'class' => 'DrupalTextMetaTag',
    'group' => 'revised',
    'element' => array(
      '#theme' => 'metatag_revised',
    ),
  );
  return $info;
}

